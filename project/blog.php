<?php

// functions


function gen($n){
    $b = array(  'lorem', 'ipsum', 'dolor', 'sit', 'amet', 'consectetur', 'adipiscing', 'elit',
            'a', 'ac', 'accumsan', 'ad', 'aenean', 'aliquam', 'aliquet', 'ante',
            'aptent', 'arcu', 'at', 'auctor', 'augue', 'bibendum', 'blandit',
            'class', 'commodo', 'condimentum', 'congue', 'consequat', 'conubia',
            'convallis', 'cras', 'cubilia', 'curabitur', 'curae', 'cursus',
            'dapibus', 'diam', 'dictum', 'dictumst', 'dignissim', 'dis', 'donec',
            'dui', 'duis', 'efficitur', 'egestas', 'eget', 'eleifend', 'elementum',
            'enim', 'erat', 'eros', 'est', 'et', 'etiam', 'eu', 'euismod', 'ex',
            'facilisi', 'facilisis', 'fames', 'faucibus', 'felis', 'fermentum',
            'feugiat', 'finibus', 'fringilla', 'fusce', 'gravida', 'habitant',
            'habitasse', 'hac', 'hendrerit', 'himenaeos', 'iaculis', 'id',
            'imperdiet', 'in', 'inceptos', 'integer', 'interdum', 'justo',
            'lacinia', 'lacus', 'laoreet', 'lectus', 'leo', 'libero', 'ligula',
            'litora', 'lobortis', 'luctus', 'maecenas', 'magna', 'magnis',
            'malesuada', 'massa', 'mattis', 'mauris', 'maximus', 'metus', 'mi',
            'molestie', 'mollis', 'montes', 'morbi', 'mus', 'nam', 'nascetur',
            'natoque', 'nec', 'neque', 'netus', 'nibh', 'nisi', 'nisl', 'non',
            'nostra', 'nulla', 'nullam', 'nunc', 'odio', 'orci', 'ornare',
            'parturient', 'pellentesque', 'penatibus', 'per', 'pharetra',
            'phasellus', 'placerat', 'platea', 'porta', 'porttitor', 'posuere',
            'potenti', 'praesent', 'pretium', 'primis', 'proin', 'pulvinar',
            'purus', 'quam', 'quis', 'quisque', 'rhoncus', 'ridiculus', 'risus',
            'rutrum', 'sagittis', 'sapien', 'scelerisque', 'sed', 'sem', 'semper',
            'senectus', 'sociosqu', 'sodales', 'sollicitudin', 'suscipit',
            'suspendisse', 'taciti', 'tellus', 'tempor', 'tempus', 'tincidunt',
            'torquent', 'tortor', 'tristique', 'turpis', 'ullamcorper', 'ultrices',
            'ultricies', 'urna', 'ut', 'varius', 'vehicula', 'vel', 'velit',
            'venenatis', 'vestibulum', 'vitae', 'vivamus', 'viverra', 'volutpat',
            'vulputate',
        );
        
    $rand=array(); 
    shuffle($b);
    
    for($i=0;$i<$n;$i++)
    {
        $rand[$i]=$b[$i];
    }
    $rand= implode(" ",$rand);
    return $rand;
    }






function  insblog($title,$blogg,$currentdate,$conn)  {
   
 
   $sql = "INSERT INTO blog (title, content, date) VALUES ('$title', '$blogg', '$currentdate')";
   $conn->exec($sql);
 
 
 
 }

 
 
 
 function  insblogtag($words,$title,$s,$conn){
   for($i=0;$i<$s;$i++) {
     $tagrl="SELECT  tags FROM tag  WHERE  tags='$words[$i]'";
     $resl = $conn->query($tagrl);
     $resl->setFetchMode(PDO::FETCH_ASSOC);
     $count = $resl->rowCount();    
     if($count==0){

       $tagsq="INSERT INTO tag (tags)  VALUES('$words[$i]')";
       $conn->exec($tagsq);
       
     }  
   
    $relquery="INSERT into blog_tag_junction(blogid,tagid) 
         SELECT bd.bid,tt.tid  FROM  blog  bd  JOIN tag tt ON bd.title = '$title' AND  tt.tags = '$words[$i]' ";
       $conn->exec($relquery);

    }
 }
 
 
 
 function getblog($conn,$op1,$offset,$n) {
     $stmt = $conn->prepare("SELECT bid,title,content,date FROM blog ORDER BY bid $op1 LIMIT $offset,$n");
     $stmt->execute();
     $data = $stmt->fetchAll();
     return $data;
     
 }
 
  
 function gettagsbybid($val,$conn) {
     $sql1="SELECT tag.tags,tag.tid FROM blog_tag_junction,tag WHERE blog_tag_junction.blogid = ? AND tag.tid=blog_tag_junction.tagid ";
     $stmt2 = $conn->prepare($sql1);
     $stmt2->execute([$val]);
     $data2 = $stmt2-> fetchAll();
     return $data2;
 }
 
 
 function getblogbyid($conn,$idval) {
     $sql = "SELECT bid,title,content,date FROM blog where bid=?";
     if(filter_var($idval, FILTER_VALIDATE_INT)){
         $stmt = $conn->prepare($sql);
         $stmt->execute([$idval]);
         $row = $stmt->fetch();
     
    }
    return $row;
 }
 
 
 function totpost($conn) {
     
     $total_pages_sql = "SELECT bid FROM blog";
     $q1 = $conn->query($total_pages_sql);
     $total_rows = $q1->rowCount();
     return $total_rows;
 }
 
 
 function upblog($title,$blogg,$idval,$conn) {
     $sql = "UPDATE blog SET title='$title',content='$blogg' where bid = $idval";
     $conn->exec($sql);
     
 }
 function  deljun($idval,$conn) {
     $del="DELETE from blog_tag_junction where blogid = $idval";
     $conn->exec($del);
 
 }
 
 function cretabs($conn1) {
     
     $tab1 = "CREATE TABLE blog (
        bid int(11) NOT NULL  PRIMARY KEY AUTO_INCREMENT,
        title varchar(150) NOT NULL,
        content blob NOT NULL,
        date date NOT NULL
          )";
     $conn1->exec($tab1);
     
     $tab2 = "CREATE TABLE `tag` (
            `tid` int(11) NOT NULL  PRIMARY KEY AUTO_INCREMENT,
             `tags` varchar(30) NOT NULL
          ) ";
     $conn1->exec($tab2);
     
     $tab3 = "CREATE TABLE `blog_tag_junction` (
              `blogid` int(11) NOT NULL,
              `tagid` int(11) NOT NULL
          ) ";
     $conn1->exec($tab3);
     echo "tables created successfully";
     
 }
 
 function dbcre($conn1,$dbname) {
     $sql = $conn1->prepare("DROP DATABASE IF EXISTS ".$dbname.";");
     $sql->execute();
     $sql1 = "CREATE DATABASE ".$dbname.";";
     $conn1->exec($sql1);
     $us1="USE ".$dbname.";";
     $conn1->exec($us1);
     
 }
 
 function reltags($tagidvalue2,$conn) {
     $total_pages_sql = "SELECT bid,title,content,date FROM blog,blog_tag_junction
                    where blog.bid=blog_tag_junction.blogid and $tagidvalue2 = blog_tag_junction.tagid";
     $q1 = $conn->query($total_pages_sql);
     return  $q1;
 }
 function relget($tagidvalue2,$op1,$offset,$n,$conn) {
     $sql = "SELECT bid,title,content,date FROM blog,blog_tag_junction
                            where blog.bid=blog_tag_junction.blogid and $tagidvalue2 = blog_tag_junction.tagid
                            ORDER BY bid $op1 LIMIT $offset,$n";
     
     if(filter_var($tagidvalue2, FILTER_VALIDATE_INT)){
         $stmt = $conn->prepare($sql);
         $stmt->execute();
         $data = $stmt->fetchAll();
     }
     return $data;
 }
 
 
 



 
 
 
 
 
 
 
 // end of functions
 




?>